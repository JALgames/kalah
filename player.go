package main

import (
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

const (
	writeWait = 10 * time.Second
	pongWait = 60 * time.Second
	pingPeriod = (pongWait * 9) / 10
	maxMessageSize = 512
)

var(
	newline = []byte{'\n'}
	space = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize: 1024,
	WriteBufferSize: 1024,
}

type Player struct{
	manager *Manager
	game *Game
	conn *websocket.Conn
	send chan string
}


func (p *Player) readPump(){
	defer func(){
		if p.game != nil {
			p.game.leave <- p
		}
		p.conn.Close()
	}()

	p.conn.SetReadLimit(maxMessageSize)
	p.conn.SetReadDeadline(time.Now().Add(pongWait))
	p.conn.SetPongHandler(func(string) error { p.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := p.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		//message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		p.processMessage(string(message))
	}
}

func (p *Player) processMessage(message string) {
	if strings.HasPrefix(message, "newgame:") {
		p.manager.wantGame <- p
	} else if strings.HasPrefix(message, "joingame:"){
		id, e := strconv.Atoi(message[len("joingame:"):])
		if e != nil {
			p.send <- "error:Error\nInvalid game ID"
			return
		}
		p.manager.joinExisting <- JoinRequest{p, id}
	} else if strings.HasPrefix(message, "move:"){
		move, e := strconv.Atoi(message[len("move:"):])
		if e != nil{
			p.send <- "error:Error\nCould not parse move"
			return
		}
		p.game.moves <- &Move{p, move}
	}
}

func (p *Player) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		p.conn.Close()
	}()
	for {
		select {
		case message, ok := <-p.send:
			p.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				p.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := p.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write([]byte(message))
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			p.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := p.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func serveWs(manager *Manager, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	player := &Player{manager: manager, game: nil, conn: conn, send: make(chan string, 32)}
	//player.game.register <- player

	go player.writePump()
	go player.readPump()
}
