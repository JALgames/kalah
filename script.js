var conn;
var ownIndex = -1;
var currentScreen = "game-selection";
var seedCounts = {};
var currentTurn = null;
window.onload = function () {
    if (window["WebSocket"]) {
        var protocol = location.protocol === 'https:' ? "wss" : "ws";
        conn = new WebSocket(protocol + "://" + document.location.host + "/ws");
        conn.onclose = function (evt) {
            error("Lost connection", "The connection to the server was closed.");
        };
        conn.onmessage = function (evt) {
            processMessage(evt.data);
        };
    } else {
        error("Could not connect", "Your browser does not support web sockets. Web sockets are required to play the game.");
    }
};

function send(msg){
    if (!conn)
        return false;
    conn.send(msg);
}

function joinGame(){
    var id = document.getElementById("join-game-id").value;
    if (id.startsWith("#"))
        id = id.substr(1);
    send("joingame:" + id)
}

function switchToScreen(screen){
    document.getElementById(currentScreen).style.display = "none";
    document.getElementById(screen).style.display = "block";
    currentScreen = screen;
}

function error(title, message){
    document.getElementById("error-title").innerText = title;
    document.getElementById("error-content").innerText = message;
    document.getElementById("error").style.display = "block";
}

function closeError(){
    document.getElementById("error").style.display = "none";
}

function processMessage(msg) {
    if (msg.startsWith("state:")) {
        state = JSON.parse(msg.substr("state:".length));
        updateState(state);
    } else if (msg.startsWith("error:")) {
        var content = msg.substr("error:".length); // Should have just used json here...
        var lineBreak = content.indexOf("\n");
        error(content.substr(0, lineBreak), content.substr(lineBreak + 1));
    } else if (msg.startsWith("gameid:")) {
        switchToScreen("game");
        var gameid = msg.substr("gameid:".length);
        document.getElementById("game-id").innerHTML = "#" + gameid;
    } else if (msg.startsWith("newplayer:")) {
        var index = msg.substr("newplayer:".length);
        document.getElementById("playerstate-" + index).innerHTML = "Ready";
    } else if (msg.startsWith("left:")) {
        var index = msg.substr("left:".length);
        document.getElementById("playerstate-" + index).innerHTML = "<i>Left game<br> (you can invite a new player to join)</i>";
    } else if (msg.startsWith("board:")) {
        board = JSON.parse(msg.substr("board:".length));
        buildBoard(board);
    } else if (msg.startsWith("turn:")) {
        if (currentTurn != null)
            document.getElementById("player-" + currentTurn).style.fontWeight = "normal";
        currentTurn = parseInt(msg.substr("turn:".length));
        document.getElementById("player-" + currentTurn).style.fontWeight = "900";

        document.getElementById("yourturn").style.display = currentTurn === ownIndex ? "block" : "none";
        document.getElementById("oppturn").style.display =  currentTurn === ownIndex ? "none" : "block";

    } else if (msg.startsWith("you:")) {
        ownIndex = parseInt(msg.substr("you:".length));
    } else {
        alert("Received unknown message: \"" + msg + "\"");
    }
}
function buildBoard(board) {
    var houses = "";
    board.houses.forEach((house) => {
        style = "left: " + 100 * house.center.x + "%; top: " + 100 * house.center.y + "%;";
        cssClass = "house";
        if (house.owner === ownIndex)
            cssClass += " own";
        houses += "<div class='" + cssClass + "' onclick='move(" + house.index + ")' id='house-" + house.index + "' style='" + style + "'>loading</div>";
    });

    document.getElementById("board").innerHTML = "<img class='background' src='" + board.background + "' />" + houses;

    var players = "";
    for (var i = 0; i < board.playercount; i++){
        you = "";
        if (i === ownIndex)
            you = " (you)";
        players += "<p id='player-" + i + "'>Player " + (i + 1) + you + ": <a id='playerstate-" + i + "'><i>Empty</i></a>";
    }
    document.getElementById("players").innerHTML = players;
}

function move(house){
    send("move:" + house);
}

function updateState(state){
    let len = state.board.seeds.length;
    let delayStep = 0.1;
    let delayRound = delayStep * len;
    for (let i = 0; i < len; i++){
        const seeds = state.board.seeds[i];
        let seedsContent = "<div class='seedbox'>";
        let newSeeds = 0, deletedSeeds = 0;
        let offset = (i + len - state.lastmove) % len;
        let style = "seed";
        if (currentTurn === ownIndex)
            style += " active";
        if (seedCounts["house-" + i] !== undefined)
        {
            let oldSeeds = seedCounts["house-" + i];
            deletedSeeds = Math.max(0, oldSeeds - seeds);
            newSeeds = Math.max(0, seeds - oldSeeds);
        }
        for (let s = 0; s < seeds - newSeeds; s++)
            seedsContent += "<div class='" + style + "'></div>";
        for (let s = 0; s < newSeeds; s++){
            let delay = offset * delayStep + s * delayRound;
            seedsContent += "<div class='" + style + " new' style='animation-delay:" + delay + "s'></div>";
        }
        for (let s = 0; s < deletedSeeds; s++){
            let delay = (deletedSeeds - s - 1) * delayStep;
            seedsContent += "<div class='" + style + " deleted' style='animation-delay:" + delay + "s'></div>";
        }
        seedCounts["house-" + i] = seeds;
        seedsContent += "</div>"
        document.getElementById("house-" + i).innerHTML = seedsContent;
    }
}