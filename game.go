package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type Game struct{
	id int

	players []*Player
	state *GameState

	join chan *Player
	leave chan *Player
	wantState chan *Player

	moves chan *Move
	turn int // Who's turn it is
	lastMove int
}

func newGame(id, h, s int) *Game { // h: number of houses, s: number of seeds per house, see wiki for details
	game := Game{
		id:      id,
		players: make([]*Player, 2),
		join:      make(chan *Player),
		leave:     make(chan *Player),
		wantState: make(chan *Player),

		moves: make(chan *Move),

		state: newGamestate(h, s),
	}
	return &game
}
func (game *Game) run() {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	for {
		select {
		case joining := <-game.join:
			freeSlotCount := game.countFreeSlots()
			if freeSlotCount == 0 {
				joining.send <- "error:Game Full\nThe game doesn't have any free slots"
				continue
			}
			slotIndex := game.indexOfNthFreeSlot(rnd.Intn(freeSlotCount))
			game.players[slotIndex] = joining
			joining.game = game

			joining.send <- "you:" + strconv.Itoa(slotIndex)
			joining.send <- "board:" + game.serializeBoard()
			joining.send <- "gameid:" + strconv.Itoa(game.id)
			for index, player := range game.players{
				if player != nil{
					joining.send <- "newplayer:" + strconv.Itoa(index)
				}
			}
			joining.send <- "turn:" + strconv.Itoa(game.turn)
			game.broadcast("newplayer:" + strconv.Itoa(slotIndex))
			joining.send <- "state:" + game.serializeState()

		case leaving := <-game.leave:
			for index, player := range game.players {
				if player == leaving {
					game.players[index] = nil
					game.broadcast("left:" + strconv.Itoa(index))
				}
			}
			leaving.game = nil
		case wantsState := <-game.wantState:
			wantsState.send <- "state:" + game.serializeState()

		case move := <-game.moves:
			if game.players[game.turn] == move.player{
				if game.state.isValidMove(move.house, game.turn) {
					result := game.state.applyMove(move.house)
					game.lastMove = move.house

					if result == MoveEnd || !game.hasAnyValidMoves(game.turn){
						if !game.nextPlayer(){
							game.sendVictory()
							return
						}
					}
					game.broadcast("turn:" +  strconv.Itoa(game.turn))
					game.broadcast("state:" + game.serializeState())


				} else {
					move.player.send <- "error:Invalid move\nYour current move is not allowed."
				}
			}else{
				move.player.send <- "error:Not your turn\nPlease wait for your opponent to make a move."
			}
		}
	}
}
func (game *Game) countFreeSlots() int {
	counter := 0
	for _, player := range game.players {
		if player == nil {
			counter++
		}
	}
	return counter
}

func(game *Game) broadcast(message string) {
	for _, player := range game.players{
		if player != nil {
			player.send <- message
		}
	}
}

func (game *Game) indexOfNthFreeSlot(n int) int {
	counter := 0
	for index, player := range game.players {
		if player == nil {
			if counter == n {
				return index
			}
			counter++
		}
	}
	return -1
}

func (game *Game) serializeBoard() string{
	var sb strings.Builder

	sb.WriteString("{")
	sb.WriteString("\"background\": \"board.svg\",")
	sb.WriteString("\"houses\": [")
	houses := len(game.state.board) / 2 - 1
	houseSpacing := 1.0 / (2.0 + float64(houses))
	for i := 0; i < houses; i++{
		x := (1.5 + float64(i)) * houseSpacing
		sb.WriteString(game.serializeHouse(i, x, 0.75))
		sb.WriteString(",")
		sb.WriteString(game.serializeHouse(2 * houses - i, x, 0.25))
		sb.WriteString(",")
	}
	sb.WriteString(game.serializeHouse(houses, houseSpacing * (float64(houses) + 1.5), 0.5))
	sb.WriteString(",")
	sb.WriteString(game.serializeHouse(2 * houses + 1, 0.5 * houseSpacing, 0.5))
	sb.WriteString("],")
	sb.WriteString("\"playercount\": " + strconv.Itoa(len(game.players)))
	sb.WriteString("}")

	return sb.String()
}

func (game *Game) serializeHouse(index int, x, y float64) string{
	var sb strings.Builder
	xf := fmt.Sprintf("%f", x)
	yf := fmt.Sprintf("%f", y)
	sb.WriteString("{")
	sb.WriteString("\"index\":" + strconv.Itoa(index) + ",")
	sb.WriteString("\"center\": {\"x\":" + xf + ", \"y\":" + yf + "}")
	sb.WriteString(",\"owner\": " + strconv.Itoa(game.state.houseMembership[index]))
	sb.WriteString(",\"isStore\": " + strconv.FormatBool(game.state.isStore[index]))
	sb.WriteString("}")
	return sb.String()
}

func (game *Game) serializeState() string{
	var sb strings.Builder

	sb.WriteString("{")
	sb.WriteString("\"board\": ")
	sb.WriteString(game.state.toJson())
	sb.WriteString(", \"lastmove\": ")
	sb.WriteString(strconv.Itoa(game.lastMove))
	sb.WriteString("}")

	return sb.String()
}

func (game *Game) nextPlayer() bool{
	for i := 1; i <= len(game.players); i++{
		player := (game.turn + i) % len(game.players)
		if game.hasAnyValidMoves(player) {
			game.turn = player
			return true
		}
	}
	return false
}

func (game *Game) sendVictory(){

	winner := -1
	mostSeeds := -1
	for playerIndex, _ := range game.players {
		seeds := game.state.getScore(playerIndex);
		if seeds > mostSeeds{
			winner = playerIndex;
			mostSeeds = seeds;
		}
	}

	for playerIndex, player := range game.players {
		if playerIndex == winner {
			player.send <- "error:Congratulations!\nYou have won the game with a total of " + strconv.Itoa(mostSeeds) + " seeds."
		} else{
			player.send <- "error:The game is over!\nUnfortunately, your opponent won with a total of " + strconv.Itoa(mostSeeds) + " seeds."
		}
	}
}

func (game *Game) hasAnyValidMoves(player int) bool{
	for i := 0; i < len(game.state.board); i++{
		if game.state.isValidMove(i, player){
			return true
		}
	}
	return false
}