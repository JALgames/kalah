package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

var suffixes = [...]string{".html", ".js", ".css", ".svg", ".woff", ".woff2", ".ttf", ".eot", ".ico"} // Only these files will be served by the server
var addr = flag.String("addr", ":8080", "http service address");

func serveHome(w http.ResponseWriter, r *http.Request){
	log.Println(r.URL);

	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed);
		return
	}
	location := r.URL.Path[1:]
	if location == "" {
		location = "index.html"
	}

	if !matchesSuffix(location) {
		http.Error(w, "This file type will not be served by the server", http.StatusForbidden)
		log.Println("Forbidden: " + r.URL.Path)
		return
	}
	http.ServeFile(w, r, location);
}

func matchesSuffix(location string) bool{
	for _, suffix := range suffixes{
		if strings.HasSuffix(location, suffix) {
			return true
		}
	}
	return false
}

func main() {
	flag.Parse()

	manager := newManager()
	go manager.run()

	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(manager, w, r)
	})
	err := http.ListenAndServe(GetPort(), nil)
	if err != nil{
		log.Fatal("ListenAndServe: ", err);
	}
}


func GetPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = "4747"
		fmt.Println("INFO: No PORT environment variable detected, defaulting to " + port)
	}
	return ":" + port
}