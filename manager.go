package main

type Manager struct {
	nextID       int
	games        []*Game
	wantGame     chan *Player
	joinExisting chan JoinRequest
}
type JoinRequest struct{
	player *Player
	gameid int
}

func newManager() *Manager {
	man := Manager{
		nextID:       1,
		games:        make([]*Game, 0),
		wantGame:     make(chan *Player, 4),
		joinExisting: make(chan JoinRequest, 4),
	}
	return &man
}

func (man *Manager) createGame() *Game{
	game := newGame(man.nextID, 6, 6)
	man.games = append(man.games, game)
	man.nextID++
	return game
}

func (man *Manager) run(){
	for{
		select{
			case newGamePlayer := <- man.wantGame:
				if newGamePlayer.game != nil {
					newGamePlayer.game.leave <- newGamePlayer
				}
				game := man.createGame()
				go game.run()
				game.join <- newGamePlayer
			case joining := <- man.joinExisting:
				game := man.findGame(joining.gameid)
				if game == nil {
					joining.player.send <- "error:Error\nUnknown game id"
				} else{
					if joining.player.game != nil {
						joining.player.game.leave <- joining.player
					}
					game.join <- joining.player
				}

		default:

		}
	}
}

func (man *Manager) findGame(id int) *Game {
	for _, game := range man.games {
		if game.id == id {
			return game
		}
	}
	return nil
}