package main

import (
	"strconv"
	"strings"
)

type GameState struct{
	board []int
	houseMembership []int
	isStore []bool
}
func newGamestate(h, s int) *GameState {

	length := (h + 1) * 2
	state := GameState{
		board:           make([]int, length),
		houseMembership: make([]int, length),
		isStore:         make([]bool, length),
	}

	state.isStore[h] = true;
	state.isStore[2 * h + 1] = true;
	for i := h + 1; i < length; i++{
		state.houseMembership[i] = 1
	}
	for i := 0; i < h; i++{
		for p := 0; p < 2; p++{
			state.board[i + p * (h + 1)] = s
		}
	}
	return &state
}

func (state *GameState) toJson() string{
	var sb strings.Builder
	sb.WriteString("{")
	sb.WriteString("\"seeds\":[")
	for _, house := range state.board[:len(state.board) - 1]{
		sb.WriteString(strconv.Itoa(house))
		sb.WriteString(",")
	}
	if len(state.board) > 0{
		sb.WriteString(strconv.Itoa(state.board[len(state.board) - 1]))
	}

	sb.WriteString("]")
	sb.WriteString("}")
	return sb.String()
}

func (state *GameState) isValidMove(house int, player int) bool {
	if house < 0 || house >= len(state.board) {
		return false
	}
	if state.board[house] == 0 {
		return false
	}
	if state.houseMembership[house] != player {
		return false
	}
	return !state.isStore[house]
}

type MoveResult bool
const(
	MoveEnd  MoveResult = false
	MoveContinue MoveResult = true
)

func (state *GameState) applyMove(house int) MoveResult{
	seeds := state.board[house]
	state.board[house] = 0
	for i := 1; i <= seeds; i++{
		state.board[(house + i) % len(state.board)]++
	}
	lastI := (house + seeds) % len(state.board)
	if state.houseMembership[lastI] == state.houseMembership[house]{
		if state.isStore[lastI]{
			return MoveContinue;
		} else if state.board[lastI] == 1{
			opposing := len(state.board) - lastI - 2
			state.board[lastI] += state.board[opposing]
			state.board[opposing] = 0
		}
	}
	return MoveEnd;
}

func (state *GameState) getScore(player int) int{
	score := 0
	for houseIndex, house := range state.board {
		if state.isStore[houseIndex] && state.houseMembership[houseIndex] == player {
			score += house
		}
	}
	return score;
}